<?php

require 'functions.php';

// $page = !empty($_GET['page']) ? $_GET['page'] : 'default';
$page = requestGet('page', 'default');

$file = "controller/{$page}.php";

if (!file_exists($file)) {
    die('404 - not found');
}

require $file;

ob_start();
require "views/{$page}.phtml";
$content = ob_get_clean();

require 'layout.phtml';