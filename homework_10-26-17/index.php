<?php

session_start();

include 'functions.php';
$counter = countVisit();
// $message = requestGet('message');
$securityNumber = rand(1000, 9999);
$editMode = null;

if (requestGet('action') == 'delete' && requestGet('id')) {

    $result = deleteComment(requestGet('id'));
    $message = $result === false ? 'Error deleting' : 'Deleted';

    redirect('/homework_10-26-17?message=' . $message);
}

if (requestGet('action') == 'edit' && requestGet('id')) {
    $editMode = requestGet('id');
}

if ($_POST) {
    if (formIsValid()) {
        $comment = createComment($_POST);
        removeSwearing($comment);
        $result = save($comment);
        $message = $result === false ? 'Error saving' : 'Saved';
        setFlash($message);

        redirect('/homework_10-26-17');
    }
    $message = 'Form is not valid';
    setFlash($message);
}

$comments = loadComments();

include 'layout.phtml';




