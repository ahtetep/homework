<?php
echo '<h1>Задание 1</h1>';

$x = rand(1, 10);
$n = rand(1, 10);
$result = $x;

for ($i = 1; $i < $n; $i++) {
    $result *= $x;
};

echo 'Результат ' . $x . '^' . $n . ' равен ' . $result;

//------------------------------------------------------

echo '<h1>Задание 2</h1>';

$p = rand(-100, 100);
$result = 1;

for ($i = 1; $i <= $p; $i++) {
    $result *= $i;
};

if($p <= 0) {
    echo 'Примечание: факториал числа которое меньше или равно нулю всегла вернет 1<br>';
};

echo 'Результат ' . $p . '! равен ' . $result;

//------------------------------------------------------

echo '<h1>Задание 3</h1>';

$number = 0;
$x = rand(1, 10);

for ($i = 1; $number <= $x; $i++) {
    $number = $number + 1 / $i;
};

echo 'Среди чисел 1, 1+1/2, 1+1/2+1/3, ... первое число, которое больше чем ' . $x . ' равно ' . $number;

//------------------------------------------------------

echo '<h1>Задание 4</h1>';

$N = 100;

for ($i = 1; $i <= $N; $i++) {
    if ($N % pow($i, 2) == 0) {
        if ($N % pow($i, 3) != 0) {
            echo $i . '<br>';
        }
    }
};

//------------------------------------------------------

echo '<h1>Задание 5</h1>';

$openingParenthesis = 0;
$closingParenthesis = 0;
$string = 'Дана строка (текст) со скобками (открывающими и закрывающими))';
$arrString = str_split($string);

foreach ($arrString as $item) {
  if ($item == '(') {
      $openingParenthesis++;
  } elseif ($item == ')') {
      $closingParenthesis++;
  }
};

if ($openingParenthesis == $closingParenthesis) {
    echo 'Скобки расставлены верно';
} else {
    echo 'Скобки расставлены не верно';
}

//------------------------------------------------------

echo '<h1>Задание 6</h1>';

$string = 'дана строка со словами. Получить массив слов (explode)';
$arrString = explode(' ', $string);
var_dump($arrString);

//------------------------------------------------------

echo '<h1>Задание 7</h1>';

$phoneNumbers = [
    '819-72-1010',
    '648-91-6269',
    '543-81-6224',
    '427-73-6755',
    '632-77-2676',
    '836-69-9934',
    '345-05-0469',
    '805-42-4188',
    '464-78-8137',
    '650-34-0361'
];

$phoneNumber = implode(', ', $phoneNumbers);

echo $phoneNumber;

//------------------------------------------------------

echo '<h1>Задание 8</h1>';

$string = 'Hello world';
$arrString = str_split($string);
$newArrString =[];

foreach ($arrString as $item) {
    if(!in_array($item , $newArrString)) {
        $newArrString[] = $item;
    }
}

echo 'После удаления всех повторяющихся символов во фразе "' . $string . '" получаем "' . implode($newArrString) . '"';

//------------------------------------------------------

echo '<h1>Задание 1</h1>';

$tags = ['html', 'css', 'php', 'js', 'jq'];

foreach ($tags as $tag) {
    echo $tag . '<br>';
}

//------------------------------------------------------

echo '<h1>Задание 2</h1>';

$numbers = [1, 20, 15, 17, 24, 35];
$result = 0;

foreach ($numbers as $number) {
    $result += $number;
}

echo '$result = ' . $result;

//------------------------------------------------------

echo '<h1>Задание 3</h1>';

$numbers = [26, 17, 136, 12, 79, 15];
$result = 0;

foreach ($numbers as $number) {
    $result += $number*$number;
}

echo '$result = ' . $result;

//------------------------------------------------------

echo '<h1>Задание 4</h1>';

$arr = ['green'=>'зеленый', 'red'=>'красный','blue'=>'голубой'];

foreach ($arr as $k => $i) {
    echo $k . '<br>';
}

foreach ($arr as $k => $i) {
    echo $i . '<br>';
}

//------------------------------------------------------

echo '<h1>Задание 5</h1>';

$arr = ['Коля' => 200, 'Вася' => 300, 'Петя' => 400];

foreach ($arr as $k => $i) {
    echo $k . ' — зарплата ' . $i . ' долларов.<br>';
}

//------------------------------------------------------

echo '<h1>Задание 6</h1>';

$arr = ['green'=>'зеленый', 'red'=>'красный','blue'=>'голубой'];
$en = [];
$ru = [];

foreach ($arr as $k => $i) {
    $en[] = $k;
    $ru[] = $i;
}

var_dump($en);
var_dump($ru);

//------------------------------------------------------

echo '<h1>Задание 7</h1>';

$numbers = [2, 5, 9, 15, 0, 4];

foreach ($numbers as $number) {
    if ($number > 3 && $number < 10) {
        echo $number . '<br>';
    }
}

//------------------------------------------------------

echo '<h1>Задание 8</h1>';

$numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];

foreach ($numbers as $number) {
    echo $number;
}

echo '<br>';

$length = count($numbers);
$i = 0;
while ($i < $length) {
    echo $numbers[$i];
    $i++;
}

echo '<br>';

for ($i = 0; $i < $length; $i++) {
    echo $numbers[$i];
}

//------------------------------------------------------

echo '<h1>Задание 9</h1>';

for ($i = 1; $i <= 10; $i++) {
    echo $i . '<br>';
}

//------------------------------------------------------

echo '<h1>Задание 10</h1>';

for ($i = 11; $i <= 22; $i++) {
    echo $i . '<br>';
}

//------------------------------------------------------

echo '<h1>Задание 11</h1>';

for ($i = 0; $i <= 10; $i++) {
    if ($i % 2 == 0 && $i != 0) {
        echo $i . '<br>';
    }
}

//------------------------------------------------------

echo '<h1>Задание 12</h1>';

$num = 0;
for ($n = 1000; $n > 50; $num++) {
    $n /= 2;
}

echo 'После деления имеем число - ' . $n . '<br>';
echo 'Количество итераций - ' . $num . '<br>';

//------------------------------------------------------

echo '<h1>Задание 13</h1>';

$tableRow = range(1, 9);
$tableColumn = range(1, 9);

echo '<table border="1" width="100%">';
foreach ($tableRow as $item) {
    echo '<tr>';
        foreach ($tableColumn as $value) {
            echo '<td>' . $item * $value . '</td>';
        }
    echo '</tr>';
}
echo '</table>';

//------------------------------------------------------

echo '<h1>Задание 14</h1>';

$arr = [4, 2, 5, 19, 13, 0, 10];
$i = null;
foreach ($arr as $e) {
    if ($e == 2 || $e == 3 || $e == 4) {
        $i = true;
    } else {
        $i = false;
    }
}

if ($i) {
    echo 'Есть!';
} else {
    echo 'Нет!';
}

//------------------------------------------------------

echo '<h1>Задание 15</h1>';

$arr = [4, 2, 5, 19, 13, 0, 10];
$count = 0;

foreach ($arr as $item) {
    $count++;
}

echo $count;

//------------------------------------------------------

echo '<h1>Задание 16</h1>';

$numbers = range(1, 9);

foreach ($numbers as $number) {
    if ($number % 3 != 0 ) {
        echo $number . ', ';
    } else {
        echo $number . '<br>';
    }
}

//------------------------------------------------------

echo '<h1>Задание 17</h1>';

$months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

$date = date('M');

foreach ($months as $month) {
    if ($month == $date) {
        echo '<b>' . $month . '</b><br>';
    } else {
        echo $month . '<br>';
    }
}

//------------------------------------------------------

echo '<h1>Задание 18</h1>';

$days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
];

foreach ($days as $day) {
    if ($day == 'Saturday' || $day == 'Sunday') {
        echo '<b>' . $day . '</b><br>';
    } else {
        echo $day . '<br>';
    }
}

//------------------------------------------------------

echo '<h1>Задание 19</h1>';

$days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
];

$dayOfTheWeek = date('l');

foreach ($days as $day) {
    if ($day == $dayOfTheWeek) {
        echo '<i>' . $day . '</i><br>';
    } else {
        echo $day . '<br>';
    }
}

//------------------------------------------------------

echo '<h1>Задание 20</h1>';

$x = 'x';
$pyramid = '';

for ($i = 1; $i <= 20; $i++) {
    $pyramid .= $x;
    echo $pyramid . '<br>';
}

//------------------------------------------------------

echo '<h1>Задание 21</h1>';

$pyramid = '';

for ($i = 1; $i <= 9; $i++) {
    $pyramid = str_repeat("$i", $i);
    echo $pyramid . '<br>';
}

//------------------------------------------------------

echo '<h1>Задание 22</h1>';

for ($xx = $pyramid = 'xx'; $pyramid != 'xxxxxxxxxx'; $pyramid .= $xx) {
    echo $pyramid . '<br>';
}

//------------------------------------------------------

echo '<h1>Задание 23</h1>';

$enteredNumber = 123;
$arrEnteredNumber = str_split($enteredNumber);
$result = 0;

foreach ($arrEnteredNumber as $item) {
    $result += $item;
}

echo 'Сумма цыфр введенного числа ' . $enteredNumber . ' равняется ' . $result;

//------------------------------------------------------

echo '<h1>Задание 24</h1>';

$enteredNumber = 442158755745;
$arrEnteredNumber = str_split($enteredNumber);
$selectedNumber = 5;
$result = 0;

foreach ($arrEnteredNumber as $item) {
    if ($item == $selectedNumber) {
        $result++;
    }
}

echo 'Цыфра ' . $selectedNumber . ' встречается в числе ' . $enteredNumber . ' ' . $result . ' раз';

//------------------------------------------------------

echo '<h1>Задание 25</h1>';

$arr = [];

for ($i = 0; $i <= 10; $i++) {
    $arr[] = rand();
}

$maxValue = max($arr);
$minValue = min($arr);

echo $maxValue . '<br>';
echo $minValue . '<br>';
var_dump($arr);

foreach ($arr as $k => $item) {
    if ($item == $maxValue) {
        $arr[$k] = $minValue;
    } elseif ($item == $minValue) {
        $arr[$k] = $maxValue;
    }
};

echo '<br>';
var_dump($arr);

//------------------------------------------------------

echo '<h1>Задание 26</h1>';

$arr = [];
$product = 1;

for ($i = 0; $i <= 10; $i++) {
    $arr[] = rand(1, 100);
}

foreach ($arr as $k => $item) {
    if ($k == 0) {
        continue;
    } elseif ($k % 2 == 0) {
        $product *= $item;
    } else {
        echo $item . '<br>';
    }
}

echo 'Произведение чисел с парными индексами равно ' . $product;

//------------------------------------------------------

echo '<h1>Задание 27</h1>';

$row = 2;
$cols = 3;
$colors = ['red','yellow','blue','gray','maroon','brown','green'];

echo '<table border="1" width="100%">';
for ($i = 1; $i <= $row; $i++) {
    echo '<tr>';
    for ($j = 1; $j <= $cols; $j++) {
        $randomR = dechex(rand(0, 255));
        $randomG = dechex(rand(0, 255));
        $randomB = dechex(rand(0, 255));
        $colorsKey = rand(0, 6);
        $num = rand();
        echo '<td bgcolor="' . $colors[$colorsKey] . '">' . $num . '</td>';
    }
    echo '</tr>';
}
echo '</table>';