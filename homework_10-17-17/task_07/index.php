<?php

include 'functions.php';

$message = requestGet('message');
$comment = file_get_contents('comment.txt');

if ($_POST) {
    $message = 'Form is not valid';

    if (formIsValid()) {
        $message = 'Form is valid';


        $file = 'comment.txt';
        $current = file_get_contents($file);
        $current .= requestPost('userName') . ': ' . requestPost('comment') . '<br>';
        file_put_contents($file, $current);


        clearForm();
        header('Location: index.php');
    }
}

include 'layout.phtml';




