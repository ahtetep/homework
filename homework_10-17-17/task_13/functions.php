<?php

function formIsValid()
{
    return !empty($_POST['phrase']);
}

function requestPost($key)
{
    if (isset($_POST[$key])) {
        return $_POST[$key];
    }

    return null;
}

function requestGet($key)
{
    if (isset($_GET[$key])) {
        return $_GET[$key];
    }

    return null;
}

function clearForm()
{
    unset($_POST);
}

function numberOfRepetitions($text)
{
    $arrText = explode(' ', $text);
    $newArrText = [];

    foreach ($arrText as $value) {
        $value = trim($value);
        if(!array_key_exists ( $value , $newArrText )) {
            if (!empty($value)){
                $newArrText[$value] = 1;
            }
        } else {
            $newArrText[$value] += 1;
        }
    }
    return $newArrText;
}

