<?php

include 'functions.php';

$message = requestGet('message');
$text = file_get_contents('text.txt');
$arrText = explode(" ", $text);
$number = requestPost('number');
$arr1 = [];
$filterArr = null;

if ($_POST) {
    $message = 'Form is not valid';

    if (formIsValid() && requestPost('number') > 0) {
        $message = 'Form is valid';


        $filterArr = filter($number, $arrText);
        if (!is_null($filterArr)) {
            $filterArr = implode(" ", $filterArr);
        } else {
            $filterArr = 'Слов с заданной длинной не найдено';
        }


        clearForm();
    }
}

include 'layout.phtml';




