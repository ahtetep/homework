<?php

function formIsValid()
{
    return !empty($_POST['number']);
}

function requestPost($key)
{
    if (isset($_POST[$key])) {
        return $_POST[$key];
    }

    return null;
}

function requestGet($key)
{
    if (isset($_GET[$key])) {
        return $_GET[$key];
    }

    return null;
}

function clearForm()
{
    unset($_POST);
}

function filter($number, array $arr, $filterArr = [])
{
    foreach ($arr as $item){
        if (preg_match("/[а-я]+/i", $item)) {
            if (strlen($item) <= $number * 2){
                $filterArr[] = $item;
            }
        }
        if (strlen($item) <= $number){
            $filterArr[] = $item;
        }
    }
    if (count($filterArr)) {
        return $filterArr;
    }
}