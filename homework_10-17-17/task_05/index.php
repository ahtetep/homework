<?php
echo '4. Написать функцию, которая выводит список файлов в заданной директории. Директория задается как параметр функции. <br>';
function outputFileList($directory, $search = null)
{
    $list = scandir($directory);
    $list = array_slice($list, '2');

    if (!is_null($search)) {
        $list = array_filter($list, function($item) use ($search) {
            return strpos($item, $search) !== false;
        });
    }
    return $list;
}

$directory = 'C:\xampp\htdocs\homework\homework_10-17-17';
$search = '01';

echo 'Список файлов в директории ' . $directory . ' которые имеют в названии "' . $search . '": ' . implode(', ', outputFileList($directory, $search));
