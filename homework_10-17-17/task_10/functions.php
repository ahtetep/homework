<?php

function formIsValid()
{
    return !empty($_POST['phrase']);
}

function requestPost($key)
{
    if (isset($_POST[$key])) {
        return $_POST[$key];
    }

    return null;
}

function requestGet($key)
{
    if (isset($_GET[$key])) {
        return $_GET[$key];
    }

    return null;
}

function clearForm()
{
    unset($_POST);
}

function uniqueWords($text)
{
    $arrText = explode(' ', $text);
    $uniqueWords = [];

    foreach ($arrText as $value){
        if (!in_array($value, $uniqueWords)) {
            $uniqueWords[] = $value;
        }
    }
    $quantity = count($uniqueWords);
    return $quantity;
}