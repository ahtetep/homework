<?php

include 'functions.php';

$message = requestGet('message');
$quantity = 0;

if ($_POST) {
    $message = 'Form is not valid';

    if (formIsValid()) {
        $message = 'Form is valid';

        $quantity = uniqueWords(requestPost('phrase'));

        clearForm();
    }
}

include 'layout.phtml';




