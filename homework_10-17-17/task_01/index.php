<?php

include 'functions.php';

$message = requestGet('message');
$sameWords = null;

if ($_POST) {
    $message = 'Form is not valid';

    if (formIsValid()) {
        $message = 'Form is valid';

        $phrase_1 = explode(" ", requestPost('phrase-1'));
        $phrase_2 = explode(" ", requestPost('phrase-2'));

        $arrayWithSameWords = getCommonWords($phrase_1, $phrase_2);
        $sameWords = implode(", ", $arrayWithSameWords);
        clearForm();
    }
}

include 'layout.phtml';




