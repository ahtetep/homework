<?php

function formIsValid()
{
    return !empty($_POST['phrase-1']) && !empty($_POST['phrase-2']);
}

function requestPost($key)
{
    if (isset($_POST[$key])) {
        return $_POST[$key];
    }

    return null;
}

function requestGet($key)
{
    if (isset($_GET[$key])) {
        return $_GET[$key];
    }

    return null;
}

function clearForm()
{
    unset($_POST);
}

function getCommonWords($a, $b) {
    return array_intersect($a, $b);
}