<?php
echo '4. Написать функцию, которая выводит список файлов в заданной директории. Директория задается как параметр функции. <br>';
function outputFileList($directory, $search = null)
{
    $list = scandir($directory);
    $list = array_slice($list, '2');

    return $list;
}

$directory = 'C:\xampp\htdocs\homework\homework_10-17-17';

echo 'Список файлов в директории ' . $directory . ': ' . implode(', ', outputFileList($directory));
