<?php

function formIsValid()
{
    return !empty($_POST['phrase']);
}

function requestPost($key)
{
    if (isset($_POST[$key])) {
        return $_POST[$key];
    }

    return null;
}

function requestGet($key)
{
    if (isset($_GET[$key])) {
        return $_GET[$key];
    }

    return null;
}

function clearForm()
{
    unset($_POST);
}

function capitalLetter($text, $chr = "utf-8")
{
    $arrText = explode('.', $text);

    $newArrText = [];
    foreach ($arrText as $value){
        $value = ltrim($value, " ");
        $newArrText[] = mb_strtoupper(mb_substr($value, 0, 1, $chr), $chr).
            mb_substr($value, 1, mb_strlen($value, $chr) - 1, $chr);
    }

    $text = implode('. ', $newArrText);
    return $text;
}

