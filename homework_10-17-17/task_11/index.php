<?php

include 'functions.php';

$message = requestGet('message');
$capitalLetter = null;

if ($_POST) {
    $message = 'Form is not valid';

    if (formIsValid()) {
        $message = 'Form is valid';

        $capitalLetter = capitalLetter(requestPost('phrase'));

        clearForm();
    }
}

include 'layout.phtml';




