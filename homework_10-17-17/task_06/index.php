<?php

include 'functions.php';

$message = requestGet('message');
$list = viewFiles();

if (formIsValid()) {

    $file = $_FILES['file']['name'];
    $tmpName = $_FILES['file']['tmp_name'];
    $path = 'gallery/' . $file;
    move_uploaded_file($tmpName, $path);

    clearForm();
}

include 'layout.phtml';



