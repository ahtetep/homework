<?php

function formIsValid()
{

    return isset($_FILES['file']['name']);
}

function requestPost($key)
{
    if (isset($_POST[$key])) {
        return $_POST[$key];
    }

    return null;
}

function requestGet($key)
{
    if (isset($_GET[$key])) {
        return $_GET[$key];
    }

    return null;
}

function clearForm()
{
    unset($_POST);
}

function viewFiles()
{
    $path = 'gallery/';
    $list = scandir($path);
    $list = array_slice($list, '2');
    return $list;
}



