<?php

include 'functions.php';

$message = requestGet('message');
$longestWords = null;
if ($_POST) {
    $message = 'Form is not valid';

    if (formIsValid()) {
        $message = 'Form is valid';

        $text = requestPost('phrase');
        $longest_string_array = lengthFiltration($text);
        $longestWords = implode(", ", $longest_string_array);

        clearForm();
    }
}

include 'layout.phtml';




