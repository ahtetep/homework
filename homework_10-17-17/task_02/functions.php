<?php

function formIsValid()
{
    return !empty($_POST['phrase']);
}

function requestPost($key)
{
    if (isset($_POST[$key])) {
        return $_POST[$key];
    }

    return null;
}

function requestGet($key)
{
    if (isset($_GET[$key])) {
        return $_GET[$key];
    }

    return null;
}

function clearForm()
{
    unset($_POST);
}

function lengthFiltration($text)
{
    $arr = explode(" ", $text);
    usort($arr,function($a, $b){
        return strlen($b)-strlen($a);
    });
    return array_slice($arr, 0, 3);
}