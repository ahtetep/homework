<?php

function formIsValid()
{
    return !empty($_POST['phrase']);
}

function requestPost($key)
{
    if (isset($_POST[$key])) {
        return $_POST[$key];
    }

    return null;
}

function requestGet($key)
{
    if (isset($_GET[$key])) {
        return $_GET[$key];
    }

    return null;
}

function clearForm()
{
    unset($_POST);
}

function reverseText($text)
{
    $arrText = explode('.', $text);

    $newArrText = array_reverse($arrText);
    $newArrText = array_diff($newArrText, array(""));
    $text = implode('. ', $newArrText);
    return $text;
}

