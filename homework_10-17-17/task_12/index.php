<?php

include 'functions.php';

$message = requestGet('message');
$reverseText = null;

if ($_POST) {
    $message = 'Form is not valid';

    if (formIsValid()) {
        $message = 'Form is valid';

        $reverseText = reverseText(requestPost('phrase'));

        clearForm();
    }
}

include 'layout.phtml';




