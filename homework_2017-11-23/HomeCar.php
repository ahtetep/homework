<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 23.11.2017
 * Time: 16:04
 */
include 'ArrClass.php';

class HomeCar
{
    const MAX_SPEED = 500;

    public $brand;
    public $model;
    private $prise;
    private $state = 'static';

    public function __construct($brand, $model)
    {
        $this->brand = $brand;
        $this->model = $model;
    }

    public function drive()
    {
        $this->state = 'moving';
    }

    public function stop()
    {
        if ($this->isMoving()) {
            echo 'You are not moving';
        } else {
            $this->state = 'static';
        }
    }

    public function isMoving()
    {
        return $this->state != 'moving';
    }

    public function setPrise($prise)
    {
        if (!is_numeric($prise)){
            die('Invalid prise');
        }
        $this->prise = $prise;
    }

    public function getPrise(){
        return $this->prise;
    }

    public function getArrHomeCar()
    {
        $result = [];
        foreach ($this as $k => $v) {
            $result[$k] = $v;
        }
        return $result;
    }

}

$car1 = new HomeCar('Mazda', '6');
$car1->setPrise(10000);
$car1->isMoving();
$car1->drive();
$arrCar1 = $car1->getArrHomeCar();

$car2 = new HomeCar('BMW', 'X10');
$car2->stop();
echo '<br>'; echo '<br>';

$arrClass = new ArrClass();
$arrClass = $arrClass->walk($car2);

var_dump($car1, $car2);
echo '<br>'; echo '<br>';

echo 'Задание: Преобразование объекта в массив. Способ 1';
echo '<pre>';
print_r($arrCar1);
echo '</pre>';

echo '<br>'; echo '<br>';
echo 'Задание: Преобразование объекта в массив. Способ 2';
echo '<pre>';
print_r($arrClass);
echo '</pre>';


