<?php

echo 'Задание 1<br>';
$name = 'Александр';

echo 'Задание 2<br>';
$age = 29;

echo 'Задание 3<br>';
echo 'Меня зовут ' . $name . '<br>';

echo 'Задание 4<br>';
echo 'Мне ' . $age . ' лет<br>';

echo 'Задание 5-8<br>';
if ($age >= 18 && $age <= 59){
    echo 'Вам еще работать и работать<br>';
} elseif ($age > 0 && $age < 18) {
    echo 'Вам еще рано работать<br>';
} elseif ($age > 59) {
    echo 'Вам пора на пенсию<br>';
} else {
    echo 'Неизвестный возраст<br>';
};

echo 'Задание 9-12<br>';
$day = rand(1, 10);
echo 'Число - ' . $day . '<br>';
switch ($day) {
    case ($day >= 1 && $day <= 5):
        echo "Это рабочий день<br>";
        break;
    case ($day == 6 || $day == 7):
        echo "Это выходной день<br>";
        break;
    default:
        echo "Неизвестный день<br>";
}
